async function process (data) {
  const linesWritten = await streamToFile(data)
  reportDone(linesWritten)
}